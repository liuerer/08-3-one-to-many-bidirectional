package com.twuc.webApp.domain.oneToMany.bidirectionalMap;

import javax.persistence.*;
import java.util.Objects;

// TODO
//
// 请使用双向映射定义 ParentEntity 和 ChildEntity 的 one-to-many 关系。其中 ChildEntity
// 的数据表应当为如下的结构。
//
// child_entity
// +───────────────────+──────────────+──────────────────────────────+
// | Column            | Type         | Additional                   |
// +───────────────────+──────────────+──────────────────────────────+
// | id                | bigint       | primary key, auto_increment  |
// | name              | varchar(20)  | not null                     |
// | parent_entity_id  | bigint       | null                         |
// +───────────────────+──────────────+──────────────────────────────+
//
// <-start-
@Entity
public class ChildEntity {
	
	@Id
	@GeneratedValue
	private Long id;
	
	@Column(nullable = false, length = 20)
	private String name;
	
	@ManyToOne
	@JoinColumn(name = "parent_entity_id")
	private ParentEntity parentEntity;
	
	public ChildEntity() {
	}
	
	public ChildEntity(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
	
	public Long getId() {
		return id;
	}
	
	public ParentEntity getParentEntity() {
		return parentEntity;
	}
	
	public void setParentEntity(ParentEntity parentEntity) {
		this.parentEntity = parentEntity;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (o == null || getClass() != o.getClass()) return false;
		ChildEntity that = (ChildEntity) o;
		return Objects.equals(name, that.name);
	}
	
	@Override
	public int hashCode() {
		return Objects.hash(name);
	}
}
// --end->
